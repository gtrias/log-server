const express = require('express')

const util = require('util')
const fs = require('fs')

const app = express()
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

const port = 3000

const writeLog = (id, payload) => {
  const filePath = `${__dirname}/logs/${id}.log`
  const fd = fs.openSync(filePath, 'a')
  const logFile = fs.createWriteStream(filePath, {flags : 'a+'});
  logFile.write(util.format(`${id}: ${JSON.stringify(payload)}`) + '\n');

  fs.closeSync(fd)
}

const logger = (id, ...args) => {
  console.log(id, ...args)
  writeLog(id, ...args)
}

app.post('/:id', (req, res) => {
  const logFileId = req.params.id
  logger(logFileId, req.body)

  res.send('ok')
})

app.listen(port, () => console.log(`log-server is linstening on port ${port}!`))
